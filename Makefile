JAVAFlAGS = -include-runtime -d
SOURCE = main.kt
OBJECT = build/main
JAROBJECT = $(OBJECT).jar

dafault = kotlinc

kotlinc:
	kotlinc $(SOURCE)

java:
	kotlinc $(SOURCE) $(JAVAFlAGS) $(JAROBJECT)

native:
	kotlinc-native $(SOURCE) -o $(OBJECT)

build:
	mkdir build

all: build native java kotlinc

clear:
	rm -r MainKt.class build/ META-INF/
